$(document).ready(function () {
    $('#formRegistration\\:btnReg').prop("disabled", true);

    $('.nav li a').on('click', function() {
        $(this).parent().parent().find('.active').removeClass('active');
        $(this).parent().addClass('active');
    });

    $("#btnClose").click(function(){
        $('#loginModal').modal("hide");
    });

    $('#btnLogin').click(function(){
        $('#msgContiner').addClass('hide');
        $('#loginModal').modal('show');
    });

    $('#formRegistration :input').blur(function(){
        console.log($(this));
        var $this = $(this);
        if($this.val()=='') {
            $this.parent().addClass("has-error");
        } else {
            $this.parent().removeClass("has-error");
        }
        checkRegForm();
    });

    $('#formRegistration :input').keyup(function(){
        console.log($(this));
        var $this = $(this);
        if($this.val() !== '') {
            $this.parent().removeClass("has-error");
        }
        checkRegForm();
    });

    var nav = function () {
        $('.gw-nav > li > a').click(function () {
            var gw_nav = $('.gw-nav');
            gw_nav.find('li').removeClass('active');
            $('.gw-nav > li > ul > li').removeClass('active');

            var checkElement = $(this).parent();
            var ulDom = checkElement.find('.gw-submenu')[0];

            if (ulDom == undefined) {
                checkElement.addClass('active');
                $('.gw-nav').find('li').find('ul:visible').slideUp();
                return;
            }
            if (ulDom.style.display != 'block') {
                gw_nav.find('li').find('ul:visible').slideUp();
                gw_nav.find('li.init-arrow-up').removeClass('init-arrow-up').addClass('arrow-down');
                gw_nav.find('li.arrow-up').removeClass('arrow-up').addClass('arrow-down');
                checkElement.removeClass('init-arrow-down');
                checkElement.removeClass('arrow-down');
                checkElement.addClass('arrow-up');
                checkElement.addClass('active');
                checkElement.find('ul').slideDown(300);
            } else {
                checkElement.removeClass('init-arrow-up');
                checkElement.removeClass('arrow-up');
                checkElement.removeClass('active');
                checkElement.addClass('arrow-down');
                checkElement.find('ul').slideUp(300);

            }
        });
        $('.gw-nav > li > ul > li > a').click(function () {
            $(this).parent().parent().parent().removeClass('active');
            $('.gw-nav > li > ul > li').removeClass('active');
            $(this).parent().addClass('active')
        });
    };
    nav();



});

function checkRegForm() {
    $('#formRegistration :input').each(function(){
        var $this = $(this);
        if($this.val()=='') {
            //$('#formRegistration\\:btnReg').addClass("disabled");
            $('#formRegistration\\:btnReg').prop("disabled", true);
            return false;
        } else {
            //$('#formRegistration\\:btnReg').removeClass("disabled");
            $('#formRegistration\\:btnReg').prop("disabled", false);


        }
    });
}

function ajaxOnEventLogin(data) {
    if (data.status == "begin") {
        loadingoverlay(true);
    } else if (data.status == "success") {
        var error = $('#loginErrorFlag').val();

        if (error === "true") {
            $('#msgContiner').removeClass('hide');
        } else {
            $('#msgContiner').addClass('hide');
        }
        loadingoverlay(false);
    }
}

function ajaxOnEventReg(data) {
    if (data.status == "begin") {
        loadingoverlay(true);

    } else if (data.status == "success") {
        var error = $('#regErrorFlag').val();

        if (error === "true") {
            $('#alertContiner').removeClass('hide').addClass("alert-danger");
        } else {
            $('#alertContiner').removeClass('hide alert-danger').addClass("alert-success");
        }
        loadingoverlay(false);
    }
}

function ajaxOnEvent(data) {
    if (data.status == "begin") {
        loadingoverlay(true);

    } else if (data.status == "success") {
        loadingoverlay(false);
    }
}

function loadingoverlay(show) {
    if(show) {
        $('#loadingOverlaySpinner').removeClass('hide');
    } else {
        $('#loadingOverlaySpinner').addClass('hide');
    }
}

function checkValue(form, message) {

    var userInput = form[form.id + ":customer_login"];

    if (userInput.value ==''){
        alert(message)
        userInput.focus();
        return false;
    }

    return true;
}