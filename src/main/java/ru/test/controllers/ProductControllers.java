package ru.test.controllers;

import ru.test.beans.*;
import ru.test.db.ConnectionH2;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by ilya on 12.08.2015.
 */
@ManagedBean(eager = true)
@SessionScoped
public class ProductControllers implements Serializable {
    private  ArrayList<Product> productList;
    private  Product currentProduct;

    public ProductControllers() {
        fillProductBySql("SELECT * FROM VIEW_PRODUCTS ORDER BY ID");
    }

    public void fillProductBySubcategoryId() {
        String subcategory_id;
        subcategory_id = SessionBean.getRequest().getParameter("subcategory_id");
        String query =  "SELECT * FROM VIEW_PRODUCTS WHERE SUBCATEGORY_ID='"+ subcategory_id +"'";
        fillProductBySql(query);
    }

    public String getProductById() {
        long id = Long.valueOf(SessionBean.getRequest().getParameter("id"));
        System.out.println("*********************************************");
        System.out.println(id);
        currentProduct = new Product();
        for(Product product : productList) {
            if (product.getId() == id) {
                currentProduct = product;
            }
        }
        System.out.println(currentProduct.getName());
        return "product";
    }

    public void fillProductBySql(String query) {
        productList = new ArrayList<Product>();
        ResultSet rs;
        Product product;
        ProductAttr attr;
        ArrayList<ProductAttr> attrList;

        long old_id = 0;

        try {
            rs = ConnectionH2.execQuery(query);
            while (rs.next()) {
                if(rs.getLong("ID") != old_id) {
                    //new product
                    old_id = rs.getLong("ID");
                    product = new Product();
                    product.setId(rs.getLong("ID"));
                    product.setName(rs.getString("PRODUCT_NAME"));
                    product.setManufacturer(rs.getString("MANUFACTURER"));
                    product.setPrice(rs.getInt("PRICE"));
                    product.setQuantity(rs.getInt("QUANTITY"));
                    product.setDescription(rs.getString("DESCRIPTION"));

                    attrList = new ArrayList<ProductAttr>();
                    attr = new ProductAttr();

                    attr.setId(rs.getLong("ATTR_ID"));
                    attr.setAttr_name(rs.getString("ATTR_NAME"));
                    attr.setAttr_vall_id(rs.getLong("ATTR_VAL_ID"));
                    attr.setVal(rs.getString("ATTR_VAL"));

                    attrList.add(attr);
                    product.setAttrs(attrList);

                    productList.add(product);

                } else {
                    old_id = rs.getLong("ID");

                    product = productList.get(productList.size() - 1);
                    attrList = product.getAttrs();
                    attr = new ProductAttr();
                    attr.setId(rs.getLong("ATTR_ID"));
                    attr.setAttr_name(rs.getString("ATTR_NAME"));
                    attr.setAttr_vall_id(rs.getLong("ATTR_VAL_ID"));
                    attr.setVal(rs.getString("ATTR_VAL"));

                    attrList.add(attr);
                }
            }
        } catch (SQLException e){
            e.printStackTrace();
        } finally {
            ConnectionH2.ConnClose();
        }
    }

    public static byte[] getImg(String id) {
        byte[] img = null;
        ResultSet rs;

        try {
            rs = ConnectionH2.execQuery("SELECT IMG FROM TBLPRODUCTIMGS WHERE PRODUCT_ID='" + id + "'");
            while (rs.next()) {
               img = rs.getBytes("IMG");
            }
            return img;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            ConnectionH2.ConnClose();
        }

    }

    public ArrayList<Product> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList<Product> productList) {
        this.productList = productList;
    }

    public Product getCurrentProduct() {
        return currentProduct;
    }

    public void setCurrentProduct(Product currentProduct) {
        this.currentProduct = currentProduct;
    }
}
