package ru.test.controllers;

import ru.test.beans.Customer;
import ru.test.beans.SessionBean;
import ru.test.db.ConnectionH2;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


/**
 * Created by ilya on 22.07.2015.
 */
@ManagedBean
@SessionScoped
public class CustomerController {
    private ArrayList<Customer> customerList;
    private Customer currentCustomer;
    private String responseMsg;
    private Boolean errorFlag;

    public CustomerController() {

    }

    public void fillCustomersAll() {
        fillCustomerBySql("select * from tblcustomers order by fio");
    }

    public void fillCustomerBySql(String query) {
        customerList = new ArrayList<Customer>();
        ResultSet rs;

        try {
            rs = ConnectionH2.execQuery(query);
            while (rs.next()) {
                Customer customer = new Customer();
                customer.setId(rs.getLong("id"));
                customer.setFio(rs.getString("fio").trim());
                customer.setLogin(rs.getString("login").trim());
                customer.setPsw(rs.getString("psw").trim());
                customer.setPhone(rs.getString("phone").trim());
                customerList.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionH2.ConnClose();
        }
    }

    public Customer getCurrentCustomer(String login) {
        currentCustomer = new Customer();
        ResultSet rs;

        try {
            rs = ConnectionH2.execQuery("select * from tblcustomers where login='" + login + "'");
            while (rs.next()) {
                Customer customer = new Customer();
                customer.setId(rs.getLong("id"));
                customer.setFio(rs.getString("fio").trim());
                customer.setLogin(rs.getString("login").trim());
                customer.setPsw(rs.getString("psw").trim());
                customer.setPhone(rs.getString("phone").trim());
                currentCustomer = customer;

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionH2.ConnClose();
        }
        return currentCustomer;
    }


    public ArrayList<Customer> getCustomerList() {
        return customerList;
    }

    public void  login() throws IOException {
        HttpServletRequest request = SessionBean.getRequest();
        String login = request.getParameter("formLogin:customerLogin");
        String psw = request.getParameter("formLogin:customerPsw");
        currentCustomer = getCurrentCustomer(login);

        responseMsg = new String();
        errorFlag = false;

        if (currentCustomer.getId() != 0) {
            if (psw.equals(currentCustomer.getPsw())) {
                HttpSession session = SessionBean.getSession();
                session.setAttribute("isAuth", "true");

                ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
                externalContext.redirect(externalContext.getRequestContextPath());
            } else {
                responseMsg = "Неверный пароль!!!";
                errorFlag = true;
            }
        } else {
            responseMsg = "Неверный логин!!!";
            errorFlag = true;
        }
    }

    public String logout() {
        HttpSession session = SessionBean.getSession();
        session.invalidate();
        return "logout";
    }

    public void registration() throws SQLException {
        errorFlag = false;

        HttpServletRequest request = SessionBean.getRequest();
        String fio = request.getParameter("formRegistration:fio");
        String login = request.getParameter("formRegistration:login").trim();
        String phone = request.getParameter("formRegistration:phone");
        String psw = request.getParameter("formRegistration:psw");
        String repeatPsw = request.getParameter("formRegistration:repeatPsw");


        Boolean existLogin = isExistLogin(login);

        if(existLogin) {
            responseMsg = "Логин уже зарегистрирован!!!";
            errorFlag = true;
        } else if (psw.equals(repeatPsw)) {
            currentCustomer.setFio(fio);
            currentCustomer.setPhone(phone);
            currentCustomer.setPsw(psw);
            currentCustomer.setLogin(login);
            Integer error =  currentCustomer.save();
            if (error == 0) {
                responseMsg = "Ошибка сохранения данных!!!";
                errorFlag = true;
            } else {
                responseMsg = "Данные успешно сохранены!!!";
                errorFlag = false;
            }
        } else {
            responseMsg = "Пароли не совпадают!!!";
            errorFlag = true;
        }

    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public Boolean getErrorFlag() {
        return errorFlag;
    }

    public Customer getCurrentCustomer() {
        return currentCustomer;
    }

    private Boolean isExistLogin(String login) {
        currentCustomer = getCurrentCustomer(login);

        if (currentCustomer.getId() == 0) {
            return false;
        } else {

            return true;
        }
    }
}


