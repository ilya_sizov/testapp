package ru.test.controllers;

import ru.test.beans.Category;
import ru.test.beans.Subcategory;
import ru.test.db.ConnectionH2;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by ilya on 22.07.2015.
 */
@ManagedBean(eager = true)
@SessionScoped
public class CategoriesController implements Serializable{
    private ArrayList<Category> categoryList;

    public CategoriesController() {
        fillCategoriesAll();
    }

    private void fillCategoriesAll() {
        categoryList = new ArrayList<Category>();
        ResultSet rs;
        Category category;
        Subcategory subcategory;
        ArrayList<Subcategory> subcategoryList;
        long old_id = 0;

        try {
            rs = ConnectionH2.execQuery("SELECT * FROM VIEW_SUBCATEGORIES ORDER BY NAME, SUBCATEGORY_NAME");
            while (rs.next()) {
                if(rs.getLong("id") != old_id) {
                    old_id = rs.getLong("id");

                    category = new Category();

                    category.setId(rs.getLong("id"));
                    category.setName(rs.getString("name"));

                    subcategory = new Subcategory();
                    subcategoryList = new ArrayList<Subcategory>();

                    subcategory.setId(rs.getLong("subcategory_id"));
                    subcategory.setCategory_id(rs.getLong("id"));
                    subcategory.setName(rs.getString("subcategory_name"));
                    subcategoryList.add(subcategory);
                    category.setSubcategories(subcategoryList);

                    categoryList.add(category);

                } else {
                    old_id = rs.getLong("id");
                    category = categoryList.get(categoryList.size() - 1);
                    subcategory = new Subcategory();
                    subcategory.setId(rs.getLong("subcategory_id"));
                    subcategory.setCategory_id(rs.getLong("id"));
                    subcategory.setName(rs.getString("subcategory_name"));

                    subcategoryList = category.getSubcategories();
                    subcategoryList.add(subcategory);
//                    category.setSubcategories(subcategoryList);
                }
            }
        } catch (SQLException e){
            e.printStackTrace();
        } finally {
            ConnectionH2.ConnClose();
        }
    }

    public ArrayList<Category> getCategoryList() {
        return categoryList;
    }
}
