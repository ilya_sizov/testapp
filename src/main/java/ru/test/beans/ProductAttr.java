package ru.test.beans;

/**
 * Created by ilya on 12.08.2015.
 */
public class ProductAttr {
    private long id;
    private String attr_name;
    private long attr_vall_id;

    public long getAttr_vall_id() {
        return attr_vall_id;
    }

    public void setAttr_vall_id(long attr_vall_id) {
        this.attr_vall_id = attr_vall_id;
    }

    private String val;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAttr_name() {
        return attr_name;
    }

    public void setAttr_name(String attr_name) {
        this.attr_name = attr_name;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
}
