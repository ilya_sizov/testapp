package ru.test.beans;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ilya on 22.07.2015.
 */
public class Category implements Serializable {
    private long id;
    private String name;
    private ArrayList<Subcategory> subcategories;

    public ArrayList<Subcategory> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(ArrayList<Subcategory> subcategories) {
        this.subcategories = subcategories;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
