package ru.test.beans;

import org.h2.jdbc.JdbcSQLException;
import ru.test.db.ConnectionH2;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.sql.Connection;
import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;

/**
 * Created by ilya on 22.07.2015.
 */
@ManagedBean
@SessionScoped
public class Customer implements Serializable {
    private long id;
    private String fio;
    private String login;
    private String psw;
    private String phone;

    public Customer() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPsw() {
        return psw;
    }

    public void setPsw(String psw) {
        this.psw = psw;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

//
//    public String login() {
//        Customer customer = checkLogin.getCustomerByLoginPsw(login, psw);
//        try {
//            if(customer.getId() != 0){
//
//                HttpSession session = SessionBean.getSession();
//                session.setAttribute("login", customer.getLogin());
//                session.setAttribute("fio", customer.getFio());
//                session.setAttribute("isSession", "true");
//               return "index";
//            } else {
//                throw new  IllegalArgumentException("Неверный логин или пароль!!!");
//            }
//        }catch (IllegalArgumentException ex){
//            FacesContext context = FacesContext.getCurrentInstance();
//            FacesMessage message = new FacesMessage(ex.getMessage());
//            message.setSeverity(FacesMessage.SEVERITY_ERROR);
//            context.addMessage("form_login", message);
//        }
//        return "login";
//    }
//
//    public String logout() {
//        HttpSession session = SessionBean.getSession();
//        session.invalidate();
//        return "index";
//    }

    public Integer save() throws SQLException {
        String queryInsert = "INSERT INTO TBLCUSTOMERS(FIO, PSW, PHONE, LOGIN) VALUES (?,?,?,?)";
        String queryUpdate;

        ArrayList<String> querydata = new ArrayList<String>();

        querydata.add(this.fio);
        querydata.add(this.psw);
        querydata.add(this.phone);
        querydata.add(this.login);


        try {
            Integer errorFlag = ConnectionH2.execUpdate(queryInsert, querydata);
            return errorFlag;
        } finally {
            ConnectionH2.ConnClose();
        }

    }
}
