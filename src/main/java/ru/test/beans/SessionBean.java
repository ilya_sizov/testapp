package ru.test.beans;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;

/**
 * Created by ilya on 23.07.2015.
 */
public class SessionBean {

    public static HttpSession getSession() {
         return  (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
    }

    public static  HttpServletRequest getRequest(){
        return  (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    }

    public static HashMap<String, Object> getSessionMap(){
        return (HashMap<String, Object> )FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    }

    public static ExternalContext getExternalContext() {
        return FacesContext.getCurrentInstance().getExternalContext();
    }

}
