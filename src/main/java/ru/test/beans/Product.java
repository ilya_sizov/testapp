package ru.test.beans;

import javax.xml.soap.Text;
import java.util.ArrayList;

/**
 * Created by ilya on 12.08.2015.
 */
public class Product {
    private long id;
    private String name;
    private long price;
    private int quantity;
    private String description;

    public void setPrice(long price) {
        this.price = price;
    }

    public int getQuantity() {

        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    public long getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    private long subcategory_id;
//    private String subcategory_name;
    private String manufacturer;
    private ArrayList<ProductAttr> attrs;



    public ArrayList<ProductAttr> getAttrs() {
        return attrs;
    }

    public void setAttrs(ArrayList<ProductAttr> attrs) {
        this.attrs = attrs;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSubcategory_id() {
        return subcategory_id;
    }

    public void setSubcategory_id(long subcategory_id) {
        this.subcategory_id = subcategory_id;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
