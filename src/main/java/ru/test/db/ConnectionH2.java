package ru.test.db;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;

import java.io.File;
import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;


/**
 * Created by ilya on 22.07.2015.
 */

@ManagedBean(eager = true)
@SessionScoped
public class ConnectionH2  {
    private static Connection conn = null;
    private static Statement stmt = null;
    private static PreparedStatement prepareStmt = null;

    public ConnectionH2() throws SQLException {
        init();
    }

    public void  init() throws SQLException {
        System.out.println("Init db");
//        File f = new File("./db/db_test.mv.db");
        File f = new File("./db/db_test.h2.db");
        try {
            Class.forName("org.h2.Driver");

            if (f.exists()) {
                System.out.println(" exist db");
            } else {
                System.out.println("Create db");
                conn = DriverManager.getConnection("jdbc:h2:file:./db/db_test;INIT=RUNSCRIPT FROM 'classpath:scripts/create.sql'", "ilya", "ilya2991");
            }
        } catch (ClassNotFoundException ex){
            ex.printStackTrace();
        } catch (SQLException ex){
            ex.printStackTrace();
        } finally {
            if (conn != null) conn.close();
        }
    }

    public static void  getConnection(){
        try {
            Class.forName("org.h2.Driver");
            conn = DriverManager.getConnection("jdbc:h2:file:./db/db_test;", "ilya", "ilya2991");

        } catch (ClassNotFoundException ex){
            ex.printStackTrace();
        } catch (SQLException ex){
            ex.printStackTrace();
        }
    }

    public static   ResultSet execQuery(String query) throws SQLException {
        getConnection();
        stmt = conn.createStatement();
        return stmt.executeQuery(query);
    }
    public static   Integer execUpdate(String query, ArrayList<String> querydata) throws SQLException {
        getConnection();
        try {
            prepareStmt = conn.prepareStatement(query);

            for(int i=0; i< querydata.size();i++){
                prepareStmt.setString(i+1, querydata.get(i));
            }

            return prepareStmt.executeUpdate();

        } catch (SQLException ex){
            ex.printStackTrace();
            return 0;
        }
    }

    public static void ConnClose(){
        try {
            if (conn != null) conn.close();
            if (stmt != null) stmt.close();
            if (prepareStmt != null) prepareStmt.close();

        }catch (SQLException  ex){
            ex.printStackTrace();
        }
    }
}
