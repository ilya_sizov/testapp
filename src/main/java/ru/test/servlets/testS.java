package ru.test.servlets;

import ru.test.beans.Customer;
import ru.test.beans.SessionBean;

import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by ilya on 23.07.2015.
 */
@WebServlet(name = "testS")
public class testS extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HashMap<String, Object> map = SessionBean.getSessionMap();
        HttpSession session = SessionBean.getSession();
        System.out.println(session.getServletContext());
    }
}
